/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "opticalcalibration.h"
#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>



namespace fs = boost::filesystem;

OpticalCalibration::OpticalCalibration()
{
    this->cmd = Settings::opticalCalibration.command;
    args = Settings::opticalCalibration.settings;
}

OpticalCalibration::~OpticalCalibration()
{

}

void OpticalCalibration::Start()
{
    LOG_INFO << " Start Optical Calibration Module ..." ;
    start = std::chrono::system_clock::now();


    std::string correctionLevel = this->args["level"];

    for ( auto &it :Settings::multidateClassificationDataList )
    {
        checkDirectory ( Settings::workspace + "/" + it.outputFolder );

        std::string opticalCalibrationOutputDir = Settings::workspace + "/" + it.outputFolder + "/" + Settings::outputSubdirs.opticalCalibration   ;
        checkDirectory ( opticalCalibrationOutputDir );

        for ( auto &image : it.imageList )
        {
            LOG_INFO << "OpticalCalibration.addInputImage( "<< image <<" )";

            fs::path imagePath ( image );

            std::string outImageName = imagePath.stem().string();
            outImageName.append ( "_" ).append ( correctionLevel ).append ( ".tif" );

            std::string outImagePath = opticalCalibrationOutputDir + +"/" + outImageName ;

            args["in"] = image;
            args["out"] = outImagePath;

            this->outputFile = outImagePath; // to check if output file is created
            this->Run();

            if ( ! this->succeed )
            {
                LOG_FATAL <<"An Error happened when performing Optical calibration for :" << image;
                std::exit ( 1 );
            }

            image = outImagePath ; // update input images
        }
    }

    end = std::chrono::system_clock::now();

    Settings::opticalCalibration.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::opticalCalibration.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::opticalCalibration.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();
    

    LOG_INFO << "End Optical Calibration Module" ;

}
