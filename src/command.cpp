/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "command.h"
#include "logger.h"
#include <pstream.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

#include "settings.h"



Command::Command()
{

}

Command::~Command()
{

}

void Command::Execute()
{

    std::string argument = "";

    for ( auto it :args )
    {
        std::string key = it.first;
        std::string value = it.second;
        if ( !value.empty () )
        {
            argument.append ( " -" ).append ( key ).append ( " " ).append ( value );
        }
    }

    std::string cmd_to_execute = cmd + argument ;

    if ( ! logFile.empty() )
    {
        std::string tmp = "stdbuf -oL ";

        tmp.append ( cmd_to_execute ).append ( " 2>&1 | tee " ).append ( logFile );

        cmd_to_execute = tmp;
    }

    LOG_TRACE << "command : " << cmd_to_execute;

    redi::ipstream proc ( cmd_to_execute,
                          redi::pstreams::pstderr );
    std::string line;
    // read child's stdout
    while ( std::getline ( proc.out (), line ) )
    {
        LOG_INFO << line ;
        //output_log+= line ;
    }
    // read child's stderr
    while ( std::getline ( proc.err (), line ) )
    {
        //std::cout << "stderr: " << line << 'n';
        //err_log +=  line;
    }
}

void Command::Run()
{

    // output file does not exist
    if ( ! boost::filesystem::exists ( outputFile ) )
    {
        this->Execute ();
    }

    else
    {
        LOG_WARNING << "Output file already exists !!  "<< outputFile   ;

        if ( Settings::isOverwrite )
        {
            LOG_WARNING << "overwrite .. ";
            this->Execute ();
        }
        else
        {
            LOG_WARNING << "Pass this step ";
        }

    }
    //check if output file is created
    if ( boost::filesystem::exists ( outputFile ) )
    {
        succeed = true;
    }
}
