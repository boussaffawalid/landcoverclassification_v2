/*
 * 	Program:     Land Cover Classification 
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 * 
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef REPORTER_H
#define REPORTER_H

#include <sstream>
#include <iostream>
#include <vector>
#include "lcctype.h"

class Reporter
{
  
 
  std::string scoreScript;
  std::string report;
  std::string reportDir ;
 

  void writehead();
  void openHTML();
  void openBody();
  void closeBody();
  void closeHTML();
  void openDiv();
  void closeDiv();

  
  void WriteTitle(std::string title, int size);
  void WriteUnderlignedTitle(std::string title, int size);
  void WriteCentredTitle(std::string title);
  void WriteTbale ( std::vector< std::string > list, std::string ColumnLabel );
  
  void WriteShapeFileTable();
  void WriteTimeTable ();
  
  void CreateScoreScript(std::vector<Importance> featuresImportance);
  void WriteScore();
  
  void writeSettings();
  void writeImage();
  void NewLine();

  
public:
Reporter();
~Reporter();

void CreateReport();
void SaveReport();
};

#endif // REPORTER_H
