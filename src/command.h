/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef PROCESSER_H
#define PROCESSER_H
#include<iostream>
#include<map>


#include <chrono>
#include <ctime>


class Command
{
protected:
    std::string cmd;
    std::map < std::string, std::string > args;
    std::string logFile;
    std::string outputFile;
    bool succeed;
    
    std::chrono::time_point<std::chrono::system_clock> start, end; 
    
    void Execute ();

public:
    void Run();
    Command();
    ~Command();

    virtual void Start() = 0;
};

#endif // PROCESSER_H
