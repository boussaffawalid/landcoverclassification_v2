/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef RASTERIZATION_H
#define RASTERIZATION_H


#include "command.h"

class Mask : public Command
{
public:
    void Start();
    Mask();
    ~Mask();
};

#endif // RASTERIZATION_H
