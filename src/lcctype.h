/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef LCCTYPE_H
#define LCCTYPE_H
#include <iostream>
#include <vector>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>  
#include <boost/serialization/string.hpp>  



enum FS_method {
    FS_ALL,
    FS_RF,
    FS_SFFS,
    FS_DOS
};

enum GENERATE_CONFIG {
    ALL_CONFIG,
    MIN_CONFIG
};


enum COLOR_MAPPING_TYPE {
    CUSTOM_COLOR_MAPPING,
    IMAGE_COLOR_MAPPING
};


typedef struct Importance {
    int channelNumber; // start from 1 because extractROIFilter->SetChannel  start from 1
    float score;
    std::string fileName;

    friend class boost::serialization::access;
    template<class Archive>
    void serialize ( Archive& ar, unsigned int ver ) {
        ar & channelNumber;
        ar & score;
        ar & fileName;
    }

} Importance;


typedef std::pair<int,float> ImportancePairType;


#endif // LCCTYPE_H  
