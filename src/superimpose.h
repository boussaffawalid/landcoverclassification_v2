/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef SUPERIMPOSE_H
#define SUPERIMPOSE_H
#include "command.h"


class Superimpose : public Command
{
public:
    Superimpose();
    ~Superimpose();

    void Start();
};

#endif // SUPERIMPOSE_H
