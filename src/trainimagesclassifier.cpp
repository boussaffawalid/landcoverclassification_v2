/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "trainimagesclassifier.h"
#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>

using namespace std;


TrainImagesClassifier::TrainImagesClassifier()
{
    this->cmd = Settings::trainImagesClassifier.command;
    args = Settings::trainImagesClassifier.settings;
}

TrainImagesClassifier::~TrainImagesClassifier()
{

}

void TrainImagesClassifier::Start()
{

    LOG_INFO << "Starting Train Images Classifier ..." ;
    start = std::chrono::system_clock::now();


    this->args["io.il"] = Settings::staticClassificationData.selectedFeatures;
    this->args["io.vd"] = Settings::staticClassificationData.trainVector;

    if ( Settings::isStatistics )
        this->args["io.imstat"] = Settings::staticClassificationData.imagesStatisticsFile ;


    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );


    std::string outputModelFile = sessionDir + "/"  + "Model." + args["classifier"];
    std::string outputLogFile = sessionDir + "/TrainImagesClassifier.log";

    this->logFile = outputLogFile;
    this->args["io.out"] = outputModelFile;


    this->outputFile = outputModelFile; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened when trying to compute training model "<<  outputModelFile ;
        std::exit ( 1 );
    }

    Settings::staticClassificationData.trainModel = outputFile;

    end = std::chrono::system_clock::now();

    Settings::trainImagesClassifier.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::trainImagesClassifier.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::trainImagesClassifier.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();
    
    LOG_INFO << "End Train Images Classifier ." ;

}
