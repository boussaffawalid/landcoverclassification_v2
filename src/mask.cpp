/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "mask.h"

#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

using namespace std;

Mask::Mask()
{
    this->cmd = Settings::rasterization.command;
    args = Settings::rasterization.settings;
}

Mask::~Mask()
{

}

void Mask::Start()
{

    fs::path inMask ( Settings::staticClassificationData.mask ) ;

    if ( boost::iequals ( inMask.extension().string(), ".shp" ) )
    {
        LOG_INFO << " Start creating mask from shape file " ;


        std::string maskDir = Settings::workspace + "/" + Settings::outputSubdirs.mask   ;
        checkDirectory ( maskDir );

        std::string outMaskName = inMask.stem().string();
        outMaskName.append ( "_mask" ).append ( ".tif" );

        std::string outMaskPath = maskDir +"/" + outMaskName ;


        this->args["in"] = Settings::staticClassificationData.mask ;
        this->args["out"] = outMaskPath ;

        //TODO Add referecne image   args["im"]

        this->outputFile = outMaskPath; // to check if output file is created
        this->Run();


        if ( ! this->succeed )
        {
            LOG_FATAL <<"An Error happened when trying to create mask from shape file: " << Settings::staticClassificationData.mask;
            std::exit ( 1 );
        }

        Settings::staticClassificationData.mask = outMaskPath ; // replace shapefile mask path with the created mask
        LOG_INFO << " End creating mask " ;
    }
}
