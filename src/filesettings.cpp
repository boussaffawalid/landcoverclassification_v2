/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "filesettings.h"
#include "logger.h"
#include "settings.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

using namespace std;
using namespace boost::algorithm;


FileSettings::FileSettings ( std::string const& file )
{
    if ( boost::filesystem::is_regular_file ( file ) )
    {
        configFile = file;
    }
    else
    {
        LOG_FATAL << "config file not found !!" ;
        std::exit ( 1 );
    }
}

FileSettings::~FileSettings()
{

}

map <string, string> FileSettings::readSettings ( string const& iniSection )
{
    map <string, string > settings;

    ini_t ini ( configFile, true );

    if ( ini.select ( iniSection ) )
    {
        auto currentSection = ini.sections.find ( iniSection );
        //currentSection->first //section names
        //currentSection->second // keys/values in section

        for ( auto j : *currentSection->second )
        {
            // j->first;   //key
            // j->second;  //value
            settings.insert ( std::make_pair ( trim_copy ( j.first ), trim_copy ( j.second ) ) );
        }
    }
    return settings;
}

void FileSettings::updateModulesSettings()
{
    for ( auto old_it : Settings::moduleList )
    {
        auto settingFromIniFile = this->readSettings ( old_it->iniSection );
        //inisection : item->iniSection
        //map to update: item->settings

        for ( auto new_it : settingFromIniFile )
        {
            if ( old_it->settings.count ( new_it.first ) == 1 )
            {
                if ( ! ( new_it.second ).empty () )
                {
                    old_it->settings[new_it.first] = new_it.second;
                }
            }
        }
    }
}

void FileSettings::updateInputDataList()
{
    ini_t ini ( configFile, true );

    if ( ini.select ( "Commun" ) )
    {
      Settings::staticClassificationData.mask = trim_copy ( ini.get ( "mask", "" ) );
      Settings::staticClassificationData.trainVector = trim_copy ( ini.get ( "trainVector", "" ) );
      Settings::staticClassificationData.testVector = trim_copy ( ini.get ( "testVector", "" ) );
    }
    else
    {
      LOG_FATAL << "Commun Section not found in the config file "  ;
      std::exit ( 1 );
    }

    boost::regex image_regex ( "(image)([^ ]*)", boost::regex::icase );


    for ( auto inputSection: Settings::inputImagesIniSection )
    {
        //check if all Settings::inputImagesIniSection exist in the file
        if (  ini.select ( inputSection ) )
        {
            LOG_TRACE << "Found section " << inputSection ;

            ini_t::sectionsit_t sectionIt = ini.sections.find ( inputSection ) ;

            Settings::MultidateClassificationData inputdata;
            inputdata.outputFolder = inputSection;
            inputdata.iniSection = inputSection; //inisection refer to date

            for ( auto j: *sectionIt->second ) //itearte over one date section key and value
            {
                string key = trim_copy ( j.first );
                string value = trim_copy ( j.second );

                LOG_TRACE << key << "=" << value ;

                if ( boost::regex_match ( key, image_regex ) )
                    inputdata.imageList.push_back ( value );
                else
                {
                    LOG_WARNING << "Invalid key: " << key << " in section " << inputSection ;
                }
            }

            Settings::multidateClassificationDataList.push_back ( inputdata );

        }
        
        else  
        {
            LOG_FATAL << "Image Section  " << inputSection << " not exist in the config file "  ;
            std::exit ( 1 );
        }
        
    }
}
