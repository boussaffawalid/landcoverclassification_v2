/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "processingchaine.h"
#include "settings.h"


ProcessingChaine::ProcessingChaine()
{

}

ProcessingChaine::~ProcessingChaine()
{

}

void ProcessingChaine::Start()
{


    if ( Settings::isGeomCheck )
    {
        _readimageinfo = ReadImageInfo();
        _readimageinfo.Start();
    }

    if ( Settings::isOpticalCalibration )
    {
        _opticalCalibration = OpticalCalibration();
        _opticalCalibration.Start();
    }

    if ( Settings::isRadiometricIndices )
    {
        _radiometricIndices = RadiometricIndices();
        _radiometricIndices.Start();
    }

    if ( Settings::isTextureExtraction )
    {
        _haralickTextureExtraction = HaralickTextureExtraction();
        _haralickTextureExtraction.Start();
    }


    if ( Settings::isClassification )
    {

        Settings::updateReferenceImage();

        _superimpose = Superimpose();
        _superimpose.Start();

        if ( Settings::isMask )
        {
            _mask = Mask();
            _mask.Start();
        }

        
        _featureSelection = FeatureSelection();
	_featureSelection.Start();

	    
 
        if ( Settings::isStatistics )
        {
            //WARNING To verify !! compute staticts from each date of after concatenate all date !!
            _computeImagesStatistics = ComputeImagesStatistics();
            _computeImagesStatistics.Start();
        }

        _trainImagesClassifier = TrainImagesClassifier();
        _trainImagesClassifier.Start();

        _imageClassifier = ImageClassifier();
        _imageClassifier.Start();


        if ( Settings::isRegularization )
        {
            classificationRegularization = ClassificationRegularization();
            classificationRegularization.Start();
        }

        _computeConfusionMatrix = ComputeConfusionMatrix();
        _computeConfusionMatrix.Start();

        _colorMapping = ColorMapping();
        _colorMapping.Start();
	
	
	_quicklook = Quicklook();
	_quicklook.Start();

    }


  
  Reporter _reporter;

   _reporter = Reporter();
  _reporter.CreateReport();
 
 
 for( auto image : Settings::inputImagesIniSection)
   std::cout <<"ini nsection" << image << std::endl;
 
 
 for( auto item : Settings::staticClassificationData.featuresImportance )
 {
    std::cout <<" image : " << item.fileName << "   |  score = " <<item.score << std::endl;
    
 }
   
 
 
}
