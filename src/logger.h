/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef LOGGER_H
#define LOGGER_H


#include "simpleLogger.h"

/**
// ===== log macros =====
#define LOG_TRACE   std::cout
#define LOG_DEBUG   std::cout
#define LOG_INFO    std::cout
#define LOG_WARNING std::cout
#define LOG_ERROR   std::cout
#define LOG_FATAL   std::cout

**/


#endif // LOGGER_H
