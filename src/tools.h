/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>

#define BOOST_NO_CXX11_SCOPED_ENUMS // to fix Boost link error – undefined reference to boost filesystem detail copy_file( )

#include <boost/filesystem.hpp>

void checkDirectory ( const std::string& directory );

int GetImageResolution ( std::string image );


bool copyDir(
    boost::filesystem::path const & source,
   boost::filesystem::path const & destination
);


#endif // TOOLS_H
