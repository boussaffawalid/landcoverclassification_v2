/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "reporter.h"
#include "tools.h"
#include "settings.h"
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <string>
#include "settings.h"
#include "logger.h"


#include <chrono>
#include <ctime>


namespace fs = boost::filesystem;
using namespace std;

Reporter::Reporter()
{
    //copy css and js file to report directory

  reportDir = Settings::workspace +"/" + Settings::outputSubdirs.session+"/report/";
  checkDirectory(reportDir);
  
  try{
  copyDir ( boost::filesystem::path ( "../share/LCC/js" ), fs::path (reportDir+"/js") ) ;
  
  copyDir ( boost::filesystem::path ( "../share/LCC/css" ), fs::path (reportDir+"/css") ) ;
  }
  catch(...)
  {
    LOG_FATAL << "couldn't copy ../share/LCC/js or ../share/LCC/js to report directory !! ";
  }
}

Reporter::~Reporter()
{

}



void Reporter::writehead()
{

    std::string text =
        "\n\n<head>\n"
        "<meta charset=\"utf-8\">\n"
        "<title>Report</title>\n"
        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
        "<meta name=\"description\" content=\"\">\n"
        "<meta name=\"LCC\" content=\"\">"

        "<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n"
        "<link href=\"css/style.css\" rel=\"stylesheet\">\n"

        "<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>\n"
        "<script type=\"text/javascript\" src=\"js/bootstrap.min.js\"></script>\n"
        "<script type=\"text/javascript\" src=\"js/scripts.js\"></script>\n"

        "<script src=\"http://code.highcharts.com/highcharts.js\"></script>\n"
        "<script src=\"http://code.highcharts.com/modules/exporting.js\"></script>\n"
        "</head>\n\n\n";

    report.append ( text ) ;

}

void Reporter::openHTML()
{
    report.append ( "<!DOCTYPE html> \n <html lang=\"en\">\n\n\n" ) ;

}
void Reporter::closeHTML()
{
    report.append ( "</html>\n\n\n" );
}

void Reporter::openBody()
{
    report.append ( "<body>\n\n\n" );
}
void Reporter::closeBody()
{
    report.append ( "</body>\n\n\n" );
}

void Reporter::CreateScoreScript ( std::vector<Importance> featuresImportance )
{

    std::string text1 =
        "\n\n<script>\n"
        "$(function chart() {\n"
        "$('#score').highcharts({\n"
        "chart: {\n"
        " type: 'column'\n"
        "},\n"
        "title: {\n"
        "text: \"Feature Selection Scores\"\n"
        "},\n"
        "subtitle: {\n"
        "text: ''\n"
        "},\n"
        "xAxis: {\n"
        " type: 'category',\n"
        "labels: {\n"
        "rotation: -45,\n"
        "style: {\n"
        " fontSize: '13px',\n"
        "fontFamily: 'Verdana, sans-serif'\n"
        "}\n"
        "}\n"
        " },\n"
        "yAxis: {\n"
        "min: 0,\n"
        " title: {\n"
        "text: 'Score'\n"
        " }\n"
        " },\n"
        "legend: {\n"
        "enabled: false\n"
        " },\n"
        "tooltip: {\n"
        " pointFormat: 'Score: <b>{point.y:.1f}</b>'\n"
        "},\n"
        " series: [{\n"
        "name: 'attribue',\n"
        " data: [\n";


    std::string text2 = "" ;

    for ( auto item : featuresImportance )
    {
        boost::filesystem::path p ( item.fileName );
        text2.append ( " [' " ).append ( p.filename().string() ).append ( "'," ).append ( std::to_string ( item.score ) ).append ( "]," ).append ( "\n" ) ;
    }

    if ( ! text2.empty() )
    {
        text2.pop_back();    //remove last "," from list
    }



    std::string text3 =
        "            ],\n"
        "dataLabels: {\n"
        "enabled: true,\n"
        "rotation: -90,\n"
        "color: '#FFFFFF',\n"
        "align: 'right',\n"
        "format: '{point.y:.1f}', // one decimal\n"
        "y: 10, // 10 pixels down from the top\n"
        "style: {\n"
        "    fontSize: '13px',\n"
        "   fontFamily: 'Verdana, sans-serif'\n"
        "}\n"
        " }\n"
        " }]\n"
        "});\n"
        "});\n"
        "</script>\n\n\n";

    scoreScript = text1 + text2 + text3;

    report.append ( scoreScript );

}

void Reporter::WriteScore()
{
    report.append ( " <div id=\"score\" style=\"min-width: 300px; height: 400px; margin: 0 auto\"></div>\n\n\n" ) ;
}

void Reporter::SaveReport ( )
{
    ofstream htmlFile;
    htmlFile.open (  reportDir + "/lcc_report.html" );
    htmlFile << report;
    htmlFile.close();

}


void Reporter::WriteUnderlignedTitle ( string title, int size )
{
    report.append ( "<h"+ std::to_string ( size ) +">" ).append ( "\n" );
    report.append ( "<u>" );
    report.append ( title ).append ( "\n" );
    report.append ( "</u>" );
    report.append ( "</h"+ std::to_string ( size ) +">" ).append ( "\n" );
}
void Reporter::WriteTitle ( string title, int size )
{
    report.append ( "<h"+ std::to_string ( size ) +">" ).append ( "\n" );
    report.append ( title ).append ( "\n" );
    report.append ( "</h"+ std::to_string ( size ) +">" ).append ( "\n" );
}


void Reporter::WriteCentredTitle ( std::string title )
{
    report.append ( "<h3 class=\"text-center text-primary\">" ).append ( "\n" );
    report.append ( title ).append ( "\n" );
    report.append ( "</h3>" ).append ( "\n" );
}

void Reporter::WriteShapeFileTable()
{
    report.append ( "<table class=\"table\">" ).append ( "\n" ) ;
    report.append ( "\t<thead>" ).append ( "\n" );
    report.append ( "\t\t<tr>" ).append ( "\n" );
    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t#" ).append ( "\n" );
    report.append ( "\t\t\t	</th>" ).append ( "\n" );
    report.append ( "\t\t\t	<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Path" ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );
    report.append ( "\t\t</tr>" ).append ( "\n" );
    report.append ( "\t</thead>\n" ).append ( "\n" );
    report.append ( "<tbody>" ).append ( "\n" );


    report.append ( "<tr class=\"active\">" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( "Train Vector" ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( Settings::staticClassificationData.trainVector ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t</tr>" ).append ( "\n" );


    report.append ( "<tr class=\"active\">" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( "Test Vector" ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( Settings::staticClassificationData.testVector ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t</tr>" ).append ( "\n" );


    report.append ( "<tr class=\"active\">" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( "Mask" ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t<td>" ).append ( "\n" );
    report.append ( "\t" ).append ( Settings::staticClassificationData.mask ).append ( "\n" );
    report.append ( "\t</td>" ).append ( "\n" );
    report.append ( "\t</tr>" ).append ( "\n" );

    report.append ( "</tbody>" ).append ( "\n" );
    report.append ( "</table>" ).append ( "\n" );

}

void Reporter::WriteTbale ( std::vector< std::string > list, std::string ColumnLabel )
{

    report.append ( "<table class=\"table\">" ).append ( "\n" ) ;
    report.append ( "\t<thead>" ).append ( "\n" );
    report.append ( "\t\t<tr>" ).append ( "\n" );
    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t#" ).append ( "\n" );
    report.append ( "\t\t\t	</th>" ).append ( "\n" );
    report.append ( "\t\t\t	<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( ColumnLabel ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );
    report.append ( "\t\t</tr>" ).append ( "\n" );
    report.append ( "\t</thead>\n" ).append ( "\n" );
    report.append ( "<tbody>" ).append ( "\n" );

    int i = 1;
    for ( auto item:  list )
    {

        report.append ( "<tr class=\"active\">" ).append ( "\n" );
        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( std::to_string ( i ) ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );
        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( item ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );
        report.append ( "\t</tr>" ).append ( "\n" );
        i++;
    }

    report.append ( "</tbody>" ).append ( "\n" );
    report.append ( "</table>" ).append ( "\n" );
}

void Reporter::writeSettings()
{
    WriteUnderlignedTitle ( "Settings" , 3 );

    report.append ( "<table class=\"table\">" ).append ( "\n" ) ;
    report.append ( "\t<thead>" ).append ( "\n" );
    report.append ( "\t\t<tr>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Module" ).append ( "\n" );
    report.append ( "\t\t\t	</th>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Status " ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );


    vector< pair<string , bool > > moduleList;


    moduleList.push_back ( {"Check images geometry", Settings::isGeomCheck} );
    moduleList.push_back ( {"Optical Calibration", Settings::isOpticalCalibration} );
    moduleList.push_back ( {"Radiometric Indices", Settings::isRadiometricIndices} );
    moduleList.push_back ( {"Texture Extraction", Settings::isTextureExtraction} );
    moduleList.push_back ( {"Classification", Settings::isClassification} );

    if ( Settings::isClassification )
    {
        moduleList.push_back ( {"Compute images Statistics", Settings::isStatistics} );
        moduleList.push_back ( {"Classification Regularization", Settings::isRegularization} );
    }


    for ( auto item : moduleList )
    {
        report.append ( "<tr class=\"active\">" ).append ( "\n" );
        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( item.first ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );
        report.append ( "\t<td>" ).append ( "\n" );

        if ( item.second )
        {
            report.append ( "\t" ).append ( "ON" ).append ( "\n" );
        }
        else
        {
            report.append ( "\t" ).append ( "OFF" ).append ( "\n" );
        }
        report.append ( "\t</td>" ).append ( "\n" );
        report.append ( "\t</tr>" ).append ( "\n" );
    }

    report.append ( "</tbody>" ).append ( "\n" );
    report.append ( "</table>" ).append ( "\n" );

}


void Reporter::WriteTimeTable()
{
    WriteUnderlignedTitle ( "Computation Time" , 3 );

    report.append ( "<table class=\"table\">" ).append ( "\n" ) ;
    report.append ( "\t<thead>" ).append ( "\n" );
    report.append ( "\t\t<tr>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Module" ).append ( "\n" );
    report.append ( "\t\t\t	</th>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Start " ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "End " ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );

    report.append ( "\t\t\t<th>" ).append ( "\n" );
    report.append ( "\t\t\t" ).append ( "Time " ).append ( "\n" );
    report.append ( "\t\t\t </th>" ).append ( "\n" );

    report.append ( "\t\t</tr>" ).append ( "\n" );
    report.append ( "\t</thead>\n" ).append ( "\n" );
    report.append ( "<tbody>" ).append ( "\n" );


    vector<Settings::ModuleSettings> moduleList;

    if ( Settings::isOpticalCalibration )
    {
        moduleList.push_back ( Settings::opticalCalibration );
    }

    if ( Settings::isRadiometricIndices )
    {
        moduleList.push_back ( Settings::radiometricIndices );
    }

    if ( Settings::isTextureExtraction )
    {
        moduleList.push_back ( Settings::textureExtraction );
    }



    if ( Settings::isClassification )
    {

        moduleList.push_back ( Settings::superimpose );
        moduleList.push_back ( Settings::featureSelection );

        if ( Settings::isStatistics )
        {
            moduleList.push_back ( Settings::imagesStatistics );
        }
        moduleList.push_back ( Settings::trainImagesClassifier );
        moduleList.push_back ( Settings::imageClassifier );
        if ( Settings::isRegularization )
        {
            moduleList.push_back ( Settings::mapRegularization ) ;
        }
        moduleList.push_back ( Settings::colorMapping ) ;
    }

    for ( auto item:  moduleList )
    {

        int time =  item.time.elapsedSeconds;
        int hour = 0;
        int min = 0;
        int sec = 0;

        hour = time/3600;
        time = time%3600;
        min = time/60;
        time = time%60;
        sec = time;

        report.append ( "<tr class=\"active\">" ).append ( "\n" );

        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( item.iniSection ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );

        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( std::ctime ( &item.time.start ) ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );

        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( std::ctime ( &item.time.end ) ).append ( "\n" );
        report.append ( "\t</td>" ).append ( "\n" );

        report.append ( "\t<td>" ).append ( "\n" );
        report.append ( "\t" ).append ( std::to_string ( hour ) ).append ( " hours and " ).append ( std::to_string ( min ) )
        .append ( " min and " ).append ( std::to_string ( sec ) ).append ( " seconds" ).append ( "\n" ) ;
        report.append ( "\t</td>" ).append ( "\n" );

        report.append ( "\t</tr>" ).append ( "\n" );
    }


    report.append ( "</tbody>" ).append ( "\n" );
    report.append ( "</table>" ).append ( "\n" );

}

void Reporter::openDiv()
{

    report.append ( "<div class=\"container\">" ).append ( "\n" );
    report.append ( "\t<div class=\"row clearfix\">" ).append ( "\n" );
    report.append ( "\t\t<div class=\"col-md-12 column\">" ).append ( "\n" );

}

void Reporter::closeDiv()
{
    report.append ( "\t\t</div>" ).append ( "\n" );
    report.append ( "\t</div>" ).append ( "\n" );
    report.append ( "</div>" ).append ( "\n" );
}


void Reporter::writeImage()
{

    boost::filesystem::copy_file ( Settings::staticClassificationData.quicklook,
                                   reportDir+"/quicklook.png",
                                   boost::filesystem::copy_option::overwrite_if_exists );

    WriteUnderlignedTitle ( "Quick look for colored map" , 3 );

    report.append ( "<img src=" );
    report.append ( "\"quicklook.png\"" ) ;
    report.append ( "width=\"80%\" height=\"80%\" alt=\"\"" );


}

void Reporter::NewLine()
{
    report.append ( "\n<br>\n" );
}


void Reporter::CreateReport()
{
    openHTML();
    writehead();
    openBody();

    if ( Settings::featureSelectionMethod == FS_RF )
    {
        CreateScoreScript ( Settings::staticClassificationData.featuresImportance );
    }
    openDiv();


    std::time_t now =  std::chrono::system_clock::to_time_t ( std::chrono::system_clock::now() );
    std::string now_s =  std::ctime ( &now );

    NewLine();
    WriteCentredTitle ( "LCC Report created on " + now_s );

    writeSettings();

    WriteUnderlignedTitle ( "INPUT VECTOR DATA LIST" , 3 );
    WriteShapeFileTable();


    NewLine();
    WriteUnderlignedTitle ( "INPUT IMAGE LIST" , 3 );
    for ( auto &it :Settings::multidateClassificationDataList )
    {
        WriteTitle ( it.iniSection , 2 );
        WriteTbale ( it.imageList, "image" );
    }

    NewLine();
    if ( Settings::isRadiometricIndices )
    {
        WriteUnderlignedTitle ( "RADIOMETRIC INDICES" , 3 );
        for ( auto &it :Settings::multidateClassificationDataList )
        {
            WriteTitle ( it.iniSection ,2 );
            WriteTbale ( it.radiometricIndicesList, "image" );
        }
    }

    NewLine();

    if ( Settings::isTextureExtraction )
    {
        WriteUnderlignedTitle ( "TEXTURE" , 3 );
        for ( auto &it :Settings::multidateClassificationDataList )
        {
            WriteTitle ( it.iniSection , 2 );
            WriteTbale ( it.textureList, "image" );
        }
    }

    NewLine();
    WriteTimeTable();
    NewLine();


    if ( Settings::featureSelectionMethod == FS_RF )
    {
        WriteScore();
    }


    NewLine();
    writeImage();
    NewLine();
    NewLine();
    NewLine();


    closeDiv();
    closeBody();
    closeHTML();

    SaveReport();
}


