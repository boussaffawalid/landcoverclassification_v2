/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "classificationregularization.h"

#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>


ClassificationRegularization::ClassificationRegularization()
{
    this->cmd = Settings::mapRegularization.command;
    args = Settings::mapRegularization.settings;
}

ClassificationRegularization::~ClassificationRegularization()
{

}

void ClassificationRegularization::Start()
{

    LOG_INFO << "Starting Classification Regularization ..." ;
    start = std::chrono::system_clock::now();

    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );

    std::string outputImage = sessionDir + "/classificationRegularized.tif";


    this->args["io.in"] = Settings::staticClassificationData.classifiedImage;

    this->args["io.out"] = outputImage;


    this->outputFile = outputImage; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened Classification Regularization module !" ;
        std::exit ( 1 );
    }

    Settings::staticClassificationData.classifiedImage = outputImage;

    end = std::chrono::system_clock::now();

    Settings::mapRegularization.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::mapRegularization.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::mapRegularization.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

        
    LOG_INFO << "End Classification Regularization" ;

}
