/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "quicklook.h"

#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"



Quicklook::Quicklook()
{
    this->cmd = Settings::quicklook.command;
    this->args = Settings::quicklook.settings;
}

Quicklook::~Quicklook()
{

}

void Quicklook::Start()
{

    LOG_INFO << "Starting Quicklook module ..." ;

    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );



    std::string output = sessionDir +"/ColorMapingQuicklook.png" ;

    this->args["in"] = Settings::staticClassificationData.colorMapingImage ;
    
    LOG_TRACE << "quicklook output file " << output ;


    this->args["out"] = output +" uint8 ";
    //this->args["out"] = output ;

    this->outputFile = output; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_WARNING <<"An Error happened when trying to create a quicklook for!"<< this->args["in"] ;

    }

    LOG_INFO << "End Quicklook" ;

    Settings::staticClassificationData.quicklook = output;
}
