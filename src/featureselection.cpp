/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "featureselection.h"


#include <iostream>

#include "otbWrapperTypes.h"

#include "otbImageFileReader.h"
#include "otbVectorDataFileReader.h"

#include "otbListSampleGenerator.h"
#include "otbVectorDataIntoImageProjectionFilter.h"
#include "otbImageList.h"
#include "otbObjectList.h"
#include "otbImageListToImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageListToVectorImageFilter.h"
#include "otbStatisticsXMLFileReader.h"
#include "otbMultiChannelExtractROI.h"
#include "otbShiftScaleSampleListFilter.h"
#include "otbImageFileWriter.h"
#include "featuresimportance.h"


#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include "lcctype.h"



#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

FeatureSelection::FeatureSelection()
{
    isRescaleData =false;
}

FeatureSelection::~FeatureSelection()
{

}



void FeatureSelection::begin()
{

    //featuresImportance.clear() ;

    std::vector<std::string > featureBandList;

    uint nbBands = 0;
    uint nbImages = 0;

    typedef otb::ImageFileReader<otb::Wrapper::FloatVectorImageType>  ImageFileReaderType;
    typedef otb::ObjectList<ImageFileReaderType>  ImageFileReaderListType;
    ImageFileReaderListType::Pointer m_ReaderList = ImageFileReaderListType::New();

    for ( int i=0; i < inputImageList.size(); i++ )
    {
        ImageFileReaderType::Pointer reader = ImageFileReaderType::New();
        reader->SetFileName ( inputImageList[i] );
        reader->UpdateOutputInformation();
        m_ReaderList->PushBack ( reader );



        int nbChannel = reader->GetOutput()->GetNumberOfComponentsPerPixel();
        if ( nbChannel == 1 )
        {
            LOG_TRACE << "nbChannel == 1 for  :" << inputImageList[i] ;
            featureBandList.push_back ( inputImageList[i] ) ;
        }
        else
        {
            LOG_TRACE << "nbChannel ==  "<< nbChannel <<" for  :" << inputImageList[i] ;
            for ( int j=1; j <= nbChannel; j++ )
            {
                std::string image_with_band = inputImageList[i]  +" band " + std::to_string ( j ) ;
                featureBandList.push_back ( image_with_band ) ;
            }
        }
    }
    m_ReaderList->UpdateOutputInformation();
    nbImages = m_ReaderList->Size();

    assert ( nbImages > 0 );

    //////////////////////////////////////////////////////////////////////////

    typedef otb::VectorDataFileReader<otb::Wrapper::VectorDataType> VectorDataFileReaderType;
    VectorDataFileReaderType::Pointer vectorReader = VectorDataFileReaderType::New();

    vectorReader->SetFileName ( shapefile );
    otb::Wrapper::VectorDataType::Pointer vectorData =  vectorReader->GetOutput();
    vectorData->Update();

    //////////////////////////////////////////////////////////////////////////////////////
    typedef otb::VectorDataIntoImageProjectionFilter<otb::Wrapper::VectorDataType,
            otb::Wrapper::FloatVectorImageType>  VectorDataReprojectionType;
    VectorDataReprojectionType::Pointer vdreproj;
    vdreproj = VectorDataReprojectionType::New();
    ///////////////////////////////////////////////////////////////////////////////


    //Sample list generator
    typedef otb::ListSampleGenerator<otb::Wrapper::FloatVectorImageType, otb::Wrapper::VectorDataType> ListSampleGeneratorType;
    ListSampleGeneratorType::Pointer sampleGenerator = ListSampleGeneratorType::New();

//////////////////////////////////////////////////////////////////////////////////////////

    typedef otb::ImageList<otb::Wrapper::FloatImageType>  ImageListType;
    typedef otb::ImageListToVectorImageFilter<ImageListType, otb::Wrapper::FloatVectorImageType > 	ListConcatenerFilterType;
    ListConcatenerFilterType::Pointer 			m_Concatener;

    ////////////////////////////////////////////////////////////////////////////////////////


    typedef otb::MultiToMonoChannelExtractROI<otb::Wrapper::FloatVectorImageType::InternalPixelType,
            otb::Wrapper::FloatImageType::PixelType>  MMExtractROIFilterType;

    typedef otb::ObjectList<MMExtractROIFilterType>          ExtractROIFilterListType;
    ExtractROIFilterListType::Pointer  			m_ExtractorList;
    /////////////////////////////////////////////////////////////////////////////////////

    ImageListType::Pointer 				m_ImageList;
    ///////////////////////////////////////////////////////////////////////////////////////////


    // Training vectordata
    typedef itk::VariableLengthVector<otb::Wrapper::FloatVectorImageType::InternalPixelType> MeasurementType;

    // Statistic XML file Reader
    typedef otb::StatisticsXMLFileReader<MeasurementType> StatisticsReader;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    typedef otb::MultiChannelExtractROI<otb::Wrapper::FloatVectorImageType::InternalPixelType,
            otb::Wrapper::FloatVectorImageType::InternalPixelType> ExtractROIFilterType;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    typedef ListSampleGeneratorType::ListSampleType ListSampleType;
    typedef ListSampleGeneratorType::ListLabelType LabelListSampleType;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    typedef otb::Statistics::ShiftScaleSampleListFilter<ListSampleType, ListSampleType> ShiftScaleFilterType;



    LOG_INFO << "nbImages = "<< nbImages  ;


    otb::Wrapper::FloatVectorImageType::Pointer image;

    if ( nbImages == 1 )
    {
        image = m_ReaderList->GetNthElement ( 0 )->GetOutput();
        image->UpdateOutputInformation();
        nbBands = image->GetNumberOfComponentsPerPixel();

        vdreproj->SetInputImage ( image );
        sampleGenerator->SetInput ( image );

    }

    else
    {
        // Split each input list  image into image
        // and generate an mono channel image list

        m_ReaderList->GetNthElement ( 0 )->GetOutput()->UpdateOutputInformation();
        otb::Wrapper::FloatVectorImageType::SizeType size =
            m_ReaderList->GetNthElement ( 0 )->GetOutput()->GetLargestPossibleRegion().GetSize();

        m_Concatener = ListConcatenerFilterType::New();
        m_ExtractorList = ExtractROIFilterListType::New();
        m_ImageList = ImageListType::New();

        for ( unsigned int i=0; i< m_ReaderList->Size(); i++ )
        {
            m_ReaderList->GetNthElement ( i )->GetOutput()->UpdateOutputInformation();

            otb::Wrapper::FloatVectorImageType::Pointer vectIm = m_ReaderList->GetNthElement ( i )->GetOutput();
            vectIm->UpdateOutputInformation();


            if ( size != vectIm->GetLargestPossibleRegion().GetSize() )
            {
                LOG_WARNING << "Input Image size mismatch..." ;
                LOG_WARNING <<"This image is skipped : " ;

            }
            else
            {
                for ( unsigned int j=0; j< vectIm->GetNumberOfComponentsPerPixel(); j++ )
                {
                    nbBands++;
                    MMExtractROIFilterType::Pointer extractor = MMExtractROIFilterType::New();
                    extractor->SetInput ( vectIm );
                    extractor->SetChannel ( j+1 );
                    m_ExtractorList->PushBack ( extractor );
                    m_ImageList->PushBack ( extractor->GetOutput() );
                }
            }
        }

        LOG_INFO << "Number of bands : " << nbBands  ;

        m_Concatener->SetInput ( m_ImageList );
        m_Concatener->UpdateOutputInformation();

        vdreproj->SetInputImage ( m_Concatener->GetOutput() );
        sampleGenerator->SetInput ( m_Concatener->GetOutput() );
    }



    vdreproj->SetInput ( vectorData );
    vdreproj->SetUseOutputSpacingAndOriginFromImage ( false );
    vdreproj->Update();

    sampleGenerator->SetInputVectorData ( vdreproj->GetOutput() );
    sampleGenerator->SetClassKey ( "Class" );
    sampleGenerator->SetMaxTrainingSize ( 1000 );
    sampleGenerator->SetMaxValidationSize ( 1000 );
    sampleGenerator->SetValidationTrainingProportion ( 0.0 );
    sampleGenerator->SetPolygonEdgeInclusion ( true );

    sampleGenerator->Update();



    //To rescale data (or not)
    MeasurementType meanMeasurementVector;
    MeasurementType stddevMeasurementVector;

    if ( isRescaleData )
    {
        StatisticsReader::Pointer statisticsReader = StatisticsReader::New();
        statisticsReader->SetFileName ( statisticsfile );
        meanMeasurementVector = statisticsReader->GetStatisticVectorByName ( "mean" );
        stddevMeasurementVector = statisticsReader->GetStatisticVectorByName ( "stddev" );
    }
    else
    {
        meanMeasurementVector.SetSize ( nbBands );
        meanMeasurementVector.Fill ( 0. );
        stddevMeasurementVector.SetSize ( nbBands );
        stddevMeasurementVector.Fill ( 1. );
    }



    ExtractROIFilterType::Pointer extractROIFilter = ExtractROIFilterType::New();

    if ( nbImages == 1 )
    {
        image->UpdateOutputInformation();
        extractROIFilter->SetStartX ( image->GetLargestPossibleRegion().GetIndex ( 0 ) );
        extractROIFilter->SetStartY ( image->GetLargestPossibleRegion().GetIndex ( 1 ) );
        extractROIFilter->SetSizeX ( image->GetLargestPossibleRegion().GetSize ( 0 ) );
        extractROIFilter->SetSizeY ( image->GetLargestPossibleRegion().GetSize ( 1 ) );
        extractROIFilter->SetInput ( image );
    }
    else
    {
        m_Concatener->GetOutput()->UpdateOutputInformation();
        extractROIFilter->SetStartX ( m_Concatener->GetOutput()->GetLargestPossibleRegion().GetIndex ( 0 ) );
        extractROIFilter->SetStartY ( m_Concatener->GetOutput()->GetLargestPossibleRegion().GetIndex ( 1 ) );
        extractROIFilter->SetSizeX ( m_Concatener->GetOutput()->GetLargestPossibleRegion().GetSize ( 0 ) );
        extractROIFilter->SetSizeY ( m_Concatener->GetOutput()->GetLargestPossibleRegion().GetSize ( 1 ) );
        extractROIFilter->SetInput ( m_Concatener->GetOutput() );
    }


    ListSampleType::Pointer trainingSample = ListSampleType::New();
    LabelListSampleType::Pointer trainingLabel = LabelListSampleType::New();

    trainingSample = sampleGenerator->GetTrainingListSample();
    trainingLabel = sampleGenerator->GetTrainingListLabel();

    // Shift scale the samples
    ShiftScaleFilterType::Pointer trainingShiftScaleFilter = ShiftScaleFilterType::New();
    trainingShiftScaleFilter->SetInput ( trainingSample );
    trainingShiftScaleFilter->SetShifts ( meanMeasurementVector );
    trainingShiftScaleFilter->SetScales ( stddevMeasurementVector );
    trainingShiftScaleFilter->Update();

    LOG_INFO << "Number of training samples: " << trainingSample->Size()  ;
    LOG_INFO << "Number of training labels : " << trainingLabel->Size()  ;

    nbcompToKeep = ( nbcompToKeep<0 ) ?nbBands:nbcompToKeep; // si -1 on prend tous les canaux
    nbcompToKeep = ( nbcompToKeep>nbBands ) ?nbBands:nbcompToKeep; // on se limite au nb de canaux de l'image !

    LOG_INFO << "Numbers of bands to keep: " << nbcompToKeep   ;

    LOG_INFO <<"Getting variable importance "  ;


    FeaturesImportance _featuresImportance = FeaturesImportance ( trainingSample, trainingLabel , featureBandList );


    if ( Settings::featureSelectionMethod == FS_RF )
    {
        LOG_INFO <<"feature selection method: random forest "  ;

        fsImportance.importance = _featuresImportance.getRandomForestImportance ( );
    }
    else if ( Settings::featureSelectionMethod == FS_SFFS )
    {
        LOG_INFO <<"feature selection method: SFFS "  ;
        fsImportance.importance = _featuresImportance.getSFFSImportance ();
    }

    else if ( Settings::featureSelectionMethod == FS_DOS )
    {
        LOG_INFO <<"feature selection method: DOS "  ;
        fsImportance.importance = _featuresImportance.getDOSImportance ( );
    }
    else if ( Settings::featureSelectionMethod == FS_ALL ) // all
    {
        LOG_INFO <<"feature selection method: KEEP ALL  "  ;
        LOG_INFO << "keep all channel number ";
    }
    else // undifiend
    {
        LOG_FATAL << "unified feature selection method : " <<  Settings::featureSelectionMethod ;
        exit ( 1 );
    }

    LOG_DEBUG << "featuresImportance.size() = " << fsImportance.importance.size();



    bool is_all_zero = true;
    for ( int i=0; i< fsImportance.importance.size(); i++ )
    {
        if ( fsImportance.importance[i].score  > 0 )
        {
            is_all_zero = false;
        }
    }

    if ( is_all_zero )
    {
        LOG_WARNING << "all features scores = 0 !!!! ---> keep all features !!! ";
    }


    //sort vector importance
    std::sort ( fsImportance.importance.begin(), fsImportance.importance.end(),
                [] ( const Importance& a, const Importance& b )
    {
        return a.score > b.score;
    } );


    if ( ( Settings::featureSelectionMethod != FS_ALL ) && !is_all_zero )
    {
        for ( int i =0; i<nbcompToKeep; i++ )
        {
            if ( fsImportance.importance[i].score > 0 )
            {
                extractROIFilter->SetChannel ( fsImportance.importance[i].channelNumber );

                LOG_INFO << "keep "<< fsImportance.importance[i].fileName   << " score = "<< fsImportance.importance[i].score  ; // vector start from 0
            }
            else
            {
                LOG_INFO << "dont keep channel number: " << fsImportance.importance[i].fileName  << " score = "<< fsImportance.importance[i].score  ;
            }

        }
    }

    //On sauvegarde l'image
    typedef otb::ImageFileWriter<otb::Wrapper::FloatVectorImageType> 	WriterType;
    WriterType::Pointer writer = WriterType::New();


    LOG_INFO << "Write file "+output  ;

    writer->SetFileName ( output );
    writer->SetInput ( extractROIFilter->GetOutput() );
    writer->Update();

}

void FeatureSelection::Start()
{
    LOG_INFO << "Starting Feature Selection ..." ;

    start = std::chrono::system_clock::now();


    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );


    nbcompToKeep = Settings::nbcompToKeep;
    this->shapefile = Settings::staticClassificationData.trainVector;
    this->output = sessionDir + "/SelectedFeatures.tif"; // iniSection refer to date //TODO we may change this


    for ( auto &it :Settings::multidateClassificationDataList )
    {
        for ( auto &image : it.imageList )
        {
            inputImageList.push_back ( image );
        }

        for ( auto &image : it.radiometricIndicesList )
        {
            inputImageList.push_back ( image );
        }

        for ( auto &image : it.textureList )
        {
            inputImageList.push_back ( image );
        }

    }

    // output file does not exist
    if ( ! boost::filesystem::exists ( output ) || ( boost::filesystem::exists ( output ) && Settings::isOverwrite ) )
    {
        this->begin();

        try
        {
            std::ofstream file ( Settings::workspace +"/" + Settings::outputSubdirs.session + "/impo.dat" );
            boost::archive::text_oarchive oa ( file );
            oa << ( const FSImportance& ) fsImportance;
        }
        catch ( ... )
        {
            LOG_FATAL << "couldn't save importance to impo.dat file  ";
        }

    }

    else
    {
        LOG_WARNING << "Output file already exists !!  "<< output   ;
        LOG_WARNING << "Pass this step ";

        try
        {
            std::ifstream file ( Settings::workspace +"/" + Settings::outputSubdirs.session + "/impo.dat" );
            boost::archive::text_iarchive ia ( file );
            ia >> fsImportance;
        }
        catch ( ... )
        {
            LOG_FATAL << "couldn't restore importance from impo.dat file ";
        }
    }

    //check if output file is created
    if ( ! boost::filesystem::exists ( output ) )
    {
        LOG_FATAL <<"An Error happened when performing features selection" ;
        std::exit ( 1 );
    }

    Settings::staticClassificationData.selectedFeatures = output;
    Settings::staticClassificationData.featuresImportance = fsImportance.importance;


    end = std::chrono::system_clock::now();

    Settings::featureSelection.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::featureSelection.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::featureSelection.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();



    LOG_INFO << "Feature Selection DONE..." ;
}
