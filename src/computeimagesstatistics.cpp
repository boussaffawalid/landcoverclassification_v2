/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "computeimagesstatistics.h"

#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>

ComputeImagesStatistics::ComputeImagesStatistics()
{
    this->cmd = Settings::imagesStatistics.command;
    this->args = Settings::imagesStatistics.settings;
}

ComputeImagesStatistics::~ComputeImagesStatistics()
{

}

void ComputeImagesStatistics::Start()
{
    LOG_INFO << "Start Compute Images Statistics ..." ;
    
    start = std::chrono::system_clock::now();

    std::string correctionLevel = this->args["level"];


    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );

    std::string statFile = sessionDir +"/imagesStatistics.xml" ;

    this->args["il"] = Settings::staticClassificationData.selectedFeatures;
    this->args["out"] = statFile;


    this->outputFile = statFile; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened when Compute Images Statistics !" ;
        std::exit ( 1 );
    }

    Settings::staticClassificationData.imagesStatisticsFile = statFile;

    end = std::chrono::system_clock::now();

    Settings::imagesStatistics.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::imagesStatistics.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::imagesStatistics.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

    LOG_INFO << "End ComputeImagesStatistics" ;
}
