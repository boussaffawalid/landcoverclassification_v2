/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "haralicktextureextraction.h"

#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>


#include "otbVectorImage.h"
#include "otbImageFileReader.h"

namespace fs = boost::filesystem;


HaralickTextureExtraction::HaralickTextureExtraction()
{
    this->cmd = Settings::textureExtraction.command;
    args = Settings::textureExtraction.settings;
}

HaralickTextureExtraction::~HaralickTextureExtraction()
{

}

void HaralickTextureExtraction::Start()
{
    LOG_INFO << " Start Haralick Texture Extraction Module ..." ;
    
    start = std::chrono::system_clock::now();


    std::string indices = this->args["list"];


    std::vector < std::string > indicesList ;

    boost::split ( indicesList, indices, boost::is_any_of ( " " ),
                   boost::token_compress_on );

    for ( auto &it :Settings::multidateClassificationDataList )
    {
        for ( auto &image : it.imageList )
        {
            checkDirectory ( Settings::workspace + "/" + it.outputFolder );
            std::string textureOutputDir = Settings::workspace + "/"  + it.outputFolder +"/" +  Settings::outputSubdirs.haralicktexture;
            checkDirectory ( textureOutputDir );


            fs::path imagePath ( image );
            std::string outImageName = imagePath.stem().string();

            int nband = GetBandNumber ( image );

            for ( int i=0; i< nband; i++ )
            {
                std::string strBandNbr = boost::lexical_cast<std::string> ( i+1 );
                std::string outputImageName;

                if ( nband == 1 )
                    outputImageName = outImageName +"_Texture.tif";
                else
                    outputImageName = outImageName +"_Texture_band_"+ strBandNbr +".tif";

                std::string outImagePath = textureOutputDir + "/" + outputImageName;

                this->args["in"] =  image ;
                this->args["out"] = outImagePath;
                this->args["channel"] = strBandNbr ;


                this->outputFile = outImagePath; // to check if output file is created
                this->Run();

                if ( ! this->succeed )
                {
                    LOG_FATAL <<"An Error happened when computing haralick texture for :" << image;
                    std::exit ( 1 );
                }

                it.textureList.push_back ( outImagePath );
            }
        }
    }
    
    
    end = std::chrono::system_clock::now();

    Settings::opticalCalibration.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::opticalCalibration.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::opticalCalibration.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();


    LOG_INFO << "End Haralick Texture Extraction" ;
}


int HaralickTextureExtraction::GetBandNumber ( const std::string& image )
{

    const unsigned int Dimension = 2;
    typedef double                                 PixelType;
    typedef otb::VectorImage<PixelType, Dimension> ImageType;
    // Software Guide : EndCodeSnippet
    // We instantiate reader
    typedef otb::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader  = ReaderType::New();
    reader->SetFileName ( image.c_str() );
    try
    {
        reader->GenerateOutputInformation();
    }
    catch ( itk::ExceptionObject& excep )
    {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
    }
    // Software Guide : EndCodeSnippet
    catch ( ... )
    {
        std::cout << "Unknown exception !" << std::endl;
        return EXIT_FAILURE;
    }

    return reader->GetOutput()->GetNumberOfComponentsPerPixel();
}

