/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "radiometricindices.h"
#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>


#include "otbVectorImage.h"
#include "otbImageFileReader.h"



namespace fs = boost::filesystem;



RadiometricIndices::RadiometricIndices()
{
    this->cmd = Settings::radiometricIndices.command;
    args = Settings::radiometricIndices.settings;

}

RadiometricIndices::~RadiometricIndices()
{

}

int RadiometricIndices::GetBandNumber ( std::string const& image )
{

    const unsigned int Dimension = 2;
    typedef double                                 PixelType;
    typedef otb::VectorImage<PixelType, Dimension> ImageType;
    // Software Guide : EndCodeSnippet
    // We instantiate reader
    typedef otb::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader  = ReaderType::New();
    reader->SetFileName ( image.c_str() );
    try
    {
        reader->GenerateOutputInformation();
    }
    catch ( itk::ExceptionObject& excep )
    {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
    }
    // Software Guide : EndCodeSnippet
    catch ( ... )
    {
        std::cout << "Unknown exception !" << std::endl;
        return EXIT_FAILURE;
    }

    return reader->GetOutput()->GetNumberOfComponentsPerPixel();
}


void RadiometricIndices::Start()
{
    LOG_INFO << " Start RadiometricIndices Module ..." ;

    start = std::chrono::system_clock::now();

    std::string indices = this->args["list"];


    std::vector < std::string > indicesList ;

    boost::split ( indicesList, indices, boost::is_any_of ( " " ),
                   boost::token_compress_on );


    for ( auto &it :Settings::multidateClassificationDataList )
    {
        for ( auto &image : it.imageList )
        {

            if ( this->GetBandNumber ( image ) == 4 ) // image is multi spectral
            {
                LOG_INFO << "RadiometricIndices.addInputImage( "<< image <<" )";

                checkDirectory ( Settings::workspace + "/" + it.outputFolder );
                std::string radiometricIndicesOutputDir = Settings::workspace + "/"  + it.outputFolder +"/" +  Settings::outputSubdirs.radiometricIndices;
                checkDirectory ( radiometricIndicesOutputDir );

                for ( auto indice : indicesList )
                {
		    
                    fs::path imagePath ( image );

                    std::string outImageName = imagePath.stem().string();

                    std::string indiceName = boost::erase_all_copy ( indice, ":" );

                    outImageName.append ( "_" ).append ( indiceName ).append ( ".tif" );

                    std::string outImagePath = radiometricIndicesOutputDir +"/" + outImageName ;

                    //TODO we may separate each indice type in a single file
                    args["in"] = image;
                    args["out"] = outImagePath;
		    args["list"] = indice; 

                    this->outputFile = outImagePath; // to check if output file is created
                    this->Run();

                    if ( ! this->succeed )
                    {
                        LOG_FATAL <<"An Error happened when computing Radiometric indices for :" << image;
                        std::exit ( 1 );
                    }

                    it.radiometricIndicesList.push_back ( outImagePath );
                }
            }
        }
    }

    
    end = std::chrono::system_clock::now();

    Settings::radiometricIndices.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::radiometricIndices.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::radiometricIndices.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

    
    LOG_INFO << "End Optical Calibration Module" ;
}
