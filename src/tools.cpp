/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "tools.h"
#include "logger.h"
#include <boost/filesystem.hpp>

#include "otbVectorImage.h"
#include "otbImageFileReader.h"


void checkDirectory ( const std::string& directory )
{

    if ( boost::filesystem::exists ( directory ) )	// does directory actually exist?
    {
        if ( boost::filesystem::is_regular_file ( directory ) )	// is  directory regular file?
        {
            LOG_ERROR << directory << " is not a directory !!! it is a file with size =  " <<  boost::filesystem::file_size ( directory )  ;
            std::exit ( 1 );
        }
        else if ( boost::filesystem::is_directory ( directory ) )
        {
            // is workspace a directory?
            LOG_DEBUG<<  directory << " is a valid directory";
        }
        else
        {
            LOG_FATAL <<" ERROR " << directory << " exists, but is neither a regular file nor a directory !!!\n" ;
        }
    }
    else
    {
        if ( boost::filesystem::create_directory ( directory ) )
        {
            LOG_WARNING << directory << " does not exist, Create it " ;

        }
        else
        {
            LOG_WARNING << directory << " does not exist, and cant create new one !! " ;
            std::exit ( 1 );
        }
    }
}

int GetImageResolution ( std::string image )
{
    const unsigned int Dimension = 2;
    typedef double                                 PixelType;
    typedef otb::VectorImage<PixelType, Dimension> ImageType;
    // Software Guide : EndCodeSnippet
    // We instantiate reader
    typedef otb::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader  = ReaderType::New();
    reader->SetFileName ( image.c_str() );
    try
    {
        reader->GenerateOutputInformation();
    }
    catch ( itk::ExceptionObject& excep )
    {
        std::cerr << "Exception caught !" << std::endl;
        std::cerr << excep << std::endl;
    }
    // Software Guide : EndCodeSnippet
    catch ( ... )
    {
        std::cout << "Unknown exception !" << std::endl;
        return EXIT_FAILURE;
    }

    return reader->GetOutput()->GetSpacing() [0];
}




bool copyDir(
    boost::filesystem::path const & source,
    boost::filesystem::path const & destination
)
{
  
    namespace fs = boost::filesystem;
    try
    {
        // Check whether the function call is valid
        if(
            !fs::exists(source) ||
            !fs::is_directory(source)
        )
        {
            std::cerr << "Source directory " << source.string()
                << " does not exist or is not a directory." << '\n'
            ;
            return false;
        }
        if(fs::exists(destination))
        {
            std::cerr << "Destination directory " << destination.string()
                << " already exists." << '\n'
            ;
            return false;
        }
        // Create the destination directory
        if(!fs::create_directory(destination))
        {
            std::cerr << "Unable to create destination directory"
                << destination.string() << '\n'
            ;
            return false;
        }
    }
    catch(fs::filesystem_error const & e)
    {
        std::cerr << e.what() << '\n';
        return false;
    }
    // Iterate through the source directory
    for(
        fs::directory_iterator file(source);
        file != fs::directory_iterator(); ++file
    )
    {
        try
        {
            fs::path current(file->path());
            if(fs::is_directory(current))
            {
                // Found directory: Recursion
                if(
                    !copyDir(
                        current,
                        destination / current.filename()
                    )
                )
                {
                    return false;
                }
            }
            else
            {
                // Found file: Copy
                fs::copy_file(
                    current,
                    destination / current.filename()
                );
            }
        }
        catch(fs::filesystem_error const & e)
        {
            std:: cerr << e.what() << '\n';
        }
    }
    return true;
}
