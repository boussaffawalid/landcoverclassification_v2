/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef FILESETTINGS_H
#define FILESETTINGS_H

#include "INI.h"
#include <map>

class FileSettings
{
    ///Declare
    typedef INI<> ini_t;
    std::string configFile;

    std::map <std::string, std::string > readSettings ( std::string const& iniSection );

public:

    FileSettings ( std::string const& file );
    virtual ~ FileSettings ();


    void updateModulesSettings();

    void updateInputDataList();
};

#endif // FILESETTINGS_H
