/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef CLASSIFICATIONREGULARIZATION_H
#define CLASSIFICATIONREGULARIZATION_H
#include "command.h"

class ClassificationRegularization : public Command
{
public:
    ClassificationRegularization();
    ~ClassificationRegularization();

    void Start();
};

#endif // CLASSIFICATIONREGULARIZATION_H
