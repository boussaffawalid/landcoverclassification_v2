/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#define BOOL_STR(b) ((b)?"Enabled":"Disabled")


#include "settings.h"
#include "logger.h"
#include "tools.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
#include <boost/format.hpp>



using namespace std;
using namespace boost::program_options;
using namespace boost::algorithm;



std::string Settings::configFile = "";
std::string Settings::workspace = "";

FS_method Settings::featureSelectionMethod = FS_ALL ;
GENERATE_CONFIG Settings::generateConfigMethod = MIN_CONFIG;
COLOR_MAPPING_TYPE Settings::colorMappingMethod = IMAGE_COLOR_MAPPING;



int Settings::nbcompToKeep = 20;

bool Settings::isOverwrite = false;
bool Settings::isMask = false;
bool Settings::isOrthoRectification = false;
bool Settings::isOpticalCalibration = false;
bool Settings::isRadiometricIndices = false;
bool Settings::isTextureExtraction = false;
bool Settings::isClassification = false;
bool Settings::isStatistics = false;
bool Settings::isGeomCheck = false;
bool Settings::isRegularization = false;


void Settings::updateReferenceImage()
{
    LOG_TRACE <<" start updateReferenceImage !! " ;

    for ( auto it :Settings::multidateClassificationDataList )
    {
        for ( auto image : it.imageList )
        {
            LOG_TRACE << "resu = "<<GetImageResolution ( image ) << " for image "<< image ;

            if ( Settings::staticClassificationData.referenceImage == "" )
                Settings::staticClassificationData.referenceImage = image;
            else
            {
                if ( GetImageResolution ( image ) > GetImageResolution ( Settings::staticClassificationData.referenceImage ) )
                    Settings::staticClassificationData.referenceImage = image;
            }
        }
    }

    LOG_TRACE <<"reference image = "<< Settings::staticClassificationData.referenceImage;
}


void Settings::SetFeatureSelectionMethod ( std::string const& val )
{
    if ( to_lower_copy ( val ) == "all" )
        featureSelectionMethod = FS_ALL;
    else if ( to_lower_copy ( val ) == "rf" )
        featureSelectionMethod = FS_RF;
    else if ( to_lower_copy ( val ) == "sffs" )
        featureSelectionMethod = FS_SFFS;
    else if ( to_lower_copy ( val ) == "dos" )
        featureSelectionMethod = FS_DOS;
    else throw boost::program_options::validation_error ( boost::program_options::validation_error::invalid_option_value,
                "--featureSelection" );
    
     //   else{
   //   std::cout<<"error in paramter GenerateParameterFile" ;// TODO THis is only temporally untill fix link error for throw boost::program_options::validation_error
   //   std::exit(1);
   // }
    
}


void Settings::SetGenerateConfigMethod ( std::string const&  val )
{
    if ( to_lower_copy ( val ) == "all" )
        generateConfigMethod = ALL_CONFIG;
    else if ( to_lower_copy ( val ) == "min" )
        generateConfigMethod = MIN_CONFIG;
    else throw boost::program_options::validation_error ( boost::program_options::validation_error::invalid_option_value,
                "--GenerateParameterFile" );

}



void Settings::SetColorMappingMethod ( std::string const&  val )
{
    if ( to_lower_copy ( val ) == "image" )
        colorMappingMethod = IMAGE_COLOR_MAPPING;
    else if ( to_lower_copy ( val ) == "custom" )
        colorMappingMethod = CUSTOM_COLOR_MAPPING;
    else throw boost::program_options::validation_error ( boost::program_options::validation_error::invalid_option_value,
                "--ColorMapping" );
    
    
}



void Settings::SetConfigFile ( const std::string& val )
{
    if ( !boost::filesystem::is_regular_file ( val ) )
    {
        LOG_FATAL << val << "  does not exist !!! " ;
        std::exit ( 1 );
    }
}

void Settings::SetWorkdir ( const std::string& val )
{
    checkDirectory ( val );
}


void Settings::Validate()
{
    bool valid = true;
    if ( multidateClassificationDataList.empty() )
    {
        LOG_FATAL << "Empty input parameter !! No Date section found in config file or all date section are empty !!" ;
        LOG_FATAL << "Please check you config file";
        std::exit ( 1 );
    }
    if ( isMask )
    {
        if ( !boost::filesystem::is_regular_file ( Settings::staticClassificationData.mask ) )
        {
            LOG_FATAL << "use mask option is true yet " <<Settings::staticClassificationData.mask << "  not found !!! " ;
            LOG_FATAL << "Please check you config file section [mask]";
            valid =false;
        }
    }

    if ( isClassification )
    {
        if ( !boost::filesystem::exists ( Settings::staticClassificationData.testVector ) )
        {
            LOG_FATAL << " Test vector not found !!!  " << Settings::staticClassificationData.testVector  ;
            valid =false;
        }
        if ( !boost::filesystem::exists ( Settings::staticClassificationData.trainVector ) )
        {
            LOG_FATAL << " Training vector not found !!!  " << Settings::staticClassificationData.trainVector  ;
            valid =false;
        }

    }
    for ( auto item : multidateClassificationDataList )
    {

        if ( isOrthoRectification || isOpticalCalibration || isRadiometricIndices || isTextureExtraction || isClassification )
        {
            for ( auto image : item.imageList )
            {
                if ( !boost::filesystem::exists ( image ) )
                {
                    LOG_FATAL << " Input image not found !!!  " <<  image ;
                    valid =false;
                }
            }

        }
    }
    if ( ! valid )
    {
        LOG_FATAL << "Required input file not found !!" ;
        std::exit ( 1 );
    }
}


void Settings::Print()
{
    //TODO

   
      cout << boost::format ( "%-20s %-20s\n" ) %"configuration file:" %configFile;
      cout << boost::format ( "%-20s %-20s\n" ) %"Work Directory:" % workspace ;

      cout << boost::format ( "%-20s %-20s\n" ) %"overwriting existing file :" % BOOL_STR ( isOverwrite ) ;

      cout << boost::format ( "%-20s %-20s\n" ) %"OpticalCalibration:" % BOOL_STR ( isOpticalCalibration ) ;

      cout << boost::format ( "%-20s %-20s\n" ) %"RadiometricIndices:" %BOOL_STR ( isRadiometricIndices )  ;


      cout << boost::format ( "%-20s %-20s\n" ) %"Classification:" %BOOL_STR ( isClassification )  ;
      if ( isClassification )
      {
          cout << boost::format ( "%-20s %-20s %-20s\n" ) %"" %"Compute statistics:" %BOOL_STR ( isStatistics )  ;

      }

    
    cout <<std::endl;
    string input;
    cout <<  "Are you sure you want to continue ? [y/n ]"<<endl;
    getline ( std::cin, input );
    
    if( std::tolower( input.at(0) ) == 'n' )
      std::exit(1);
}



std::vector< Settings::ModuleSettings* > Settings::moduleList = {&Settings::opticalCalibration, &Settings::radiometricIndices};

std::vector< Settings::MultidateClassificationData> Settings::multidateClassificationDataList = { };

std::vector< std::string > Settings::inputImagesIniSection= {};


Settings::StaticClassificationData Settings::staticClassificationData=
{
    "",
    "",
    "",
    "",
    "",
    ""
};

Settings::OutputSubdirs Settings::outputSubdirs =
{
    "OpticalCalibration",
    "RadiometricIndices",
    "HaralickTexture",
    "mask",
    "session"
};



/////////////////////////////////////////////////////////////

std::time_t  initTime(0) ;

Settings::ModuleSettings Settings::opticalCalibration =
{
    "otbcli_OpticalCalibration",
    "OpticalCalibration",
    {   { "in", "" }, { "out", "" }, { "level", "toa" }, { "milli", "1" }, { "clamp", "" },
        { "rsr", "" }, { "atmo.aerosol", "" }, { "atmo.oz", "" },{ "atmo.wa", "" },
        { "atmo.pressure", "" }, { "atmo.opt", "" }, { "atmo.aeronet", "" }, { "radius","" }
    }
    , {initTime,initTime,0}
};



Settings::ModuleSettings Settings::concatenateImages =
{
    "otbcli_ConcatenateImages",
    "ConcatenateImages",
    { { "il", "" }, { "out", "" } }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::textureExtraction =
{
    "otbcli_HaralickTextureExtraction",
    "TextureExtraction",
    {   { "in", "" },  {"channel",""},  {"parameters.xrad",""},  {"parameters.yrad",""},
        {"parameters.xoff",""},  {"parameters.yoff",""},  {"parameters.min",""},  {"parameters.max",""},  {"parameters.nbbin",""},
        {"texture",""},  {"out",""}
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::rasterization =
{
    "otbcli_Rasterization",
    "Rasterization",
    {{ "in", "" } , { "out", "" } , { "im", "" }, { "background", "255" } , { "mode.binary.foreground", "0" } }

  , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::radiometricIndices =
{
    "otbcli_RadiometricIndices",
    "RadiometricIndice",
    {   {"in",""}, {"out",""}, {"channels.blue","3"},
        {"channels.green","2"}, {"channels.red","1"},
        {"channels.nir","4"}, {"channels.mir",""}, {"list",""}
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::readImageInfo =
{
    "otbcli_ReadImageInfo",
    "ReadImageInfo",
    { { "in", "" }, { "keywordlist", ""}, {"outkwl", ""} }
, {initTime,initTime,0}
};
Settings::ModuleSettings Settings::splitImage =
{
    "otbcli_SplitImage",
    "SplitImage",
    { { "in", "" }, { "out", "" } }
    , {initTime,initTime,0}
};
Settings::ModuleSettings Settings::superimpose =
{
    "otbcli_Superimpose",
    "Superimpose",
    {   { "inr", "" }, { "inm", "" }, { "out", "" },
        { "elev.geoid", "" }, { "elev.default", "" }, { "lms", "" }, { "interpolator", "" },
        { "interpolator.bco.radius", "" }
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::mapRegularization =
{
    "otbcli_ClassificationMapRegularization",
    "MapRegularization",
    {   { "io.in", "" }, { "io.out", "" }, { "ip.radius", "" },
        { "ip.suvbool", "" }, { "ip.nodatalabel", "" }, { "ip.undecidedlabel", "" }
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::colorMapping =
{
    "otbcli_ColorMapping",
    "ColorMapping",
    { { "in", "" }, { "out", "" }, { "method", "image" }, { "method.custom.lut", "" }, {"method.image.in",""} }

  , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::confusionMatrix =
{
    "otbcli_ComputeConfusionMatrix",
    "ConfusionMatrix",
    {   { "in", "" }, { "out", "" }, {
            "ref",
            "vector"
        }, { "ref.raster.in",
            ""
        },
        { "ref.vector.in", "" }, { "ref.vector.field", "" }, { "nodatalabel", "" }
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::imagesStatistics =
{
    "otbcli_ComputeImagesStatistics",
    "ImagesStatistics",
    { { "il", "" }, {  "out", "" },  {"bv",""} }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::featureSelection =
{
    "otbcli_FeatureSelection",
    "FeatureSelection",
    {   { "io.il", "" }, { "io.vd", "" }, { "io.imstat ", "" },
        { "sample.vfn", "Class" }, { "method", "" }, { "io.out", "" }, {"nbcomp",""}
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::imageClassifier =
{
    "otbcli_ImageClassifier",
    "ImageClassifier",
    {   { "in", "" }, { "mask", "" }, { "model", "" },
        { "imstat","" }, { "out", "" }
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::trainImagesClassifier =
{
    "otbcli_TrainImagesClassifier",
    "TrainImagesClassifier",
    {   { "io.il", "" },  { "io.vd", "" },  {
            "io.imstat",
            ""
        },
        { "io.confmatout", "" },  { "io.out", "" },  { "elev.dem", "" },  {
            "elev.geoid",
            ""
        },
        { "elev.default", "" },  { "sample.mt", "" },  { "sample.mv", "" },  {
            "sample.edg",
            ""
        },
        { "sample.vtr", "" },  { "sample.vfn", "" },  {
            "classifier",
            "libsvm"
        },  { "classifier.libsvm.k",
            ""
        },
        { "classifier.libsvm.c", "" },  {
            "classifier.libsvm.opt",
            ""
        },  { "classifier.svm.m",
            ""
        },  { "classifier.svm.k",
            ""
        },  { "classifier.svm.c",
            ""
        },  { "classifier.svm.nu",
            ""
        },
        { "classifier.svm.coef0", "" },  {
            "classifier.svm.gamma",
            ""
        },  { "classifier.svm.degree",
            ""
        },  { "classifier.svm.opt",
            ""
        },  { "classifier.boost.t",
            ""
        },  { "classifier.boost.w",
            ""
        },
        { "classifier.boost.r", "" },  {
            "classifier.boost.m",
            ""
        },  { "classifier.dt.max",
            ""
        },  { "classifier.dt.min",
            ""
        },  { "classifier.dt.ra",
            ""
        },  { "classifier.dt.cat",
            ""
        },  { "classifier.dt.f",
            ""
        },
        { "classifier.dt.r", "" },  { "classifier.dt.t", "" },  {
            "classifier.gbt.w",
            ""
        },  { "classifier.gbt.s",
            ""
        },
        { "classifier.gbt.p", "" },  { "classifier.gbt.max", "" },  {
            "classifier.ann.t",
            ""
        },
        { "classifier.ann.sizes", "" },  { "classifier.ann.f", "" },  {
            "classifier.ann.a",
            ""
        },
        { "classifier.ann.b", "" },  {
            "classifier.ann.bpdw",
            ""
        },  { "classifier.ann.bpms",
            ""
        },  { "classifier.ann.rdw",
            ""
        },  { "classifier.ann.rdwm",
            ""
        },  { "classifier.ann.term",
            ""
        },  { "classifier.ann.eps",
            ""
        },
        { "classifier.ann.iter", "" },  {
            "classifier.rf.max",
            ""
        },  { "classifier.rf.min",
            ""
        },  { "classifier.rf.ra",
            ""
        },  { "classifier.rf.cat",
            ""
        },  { "classifier.rf.var",
            ""
        },
        { "classifier.rf.nbtrees", "" },  {
            "classifier.rf.acc",
            ""
        },  { "classifier.knn.k", "" },  { "rand", "" }
    }
    , {initTime,initTime,0}
};

Settings::ModuleSettings Settings::quicklook =
{
    "otbcli_Quicklook",
    "Quicklook",
    {   { "in", "" }, { "out", "" }, { "sr", "" },
        { "cl","" }
    }
    , {initTime,initTime,0}
};


/////////////////////////////////////////////////////////
