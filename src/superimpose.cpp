/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "superimpose.h"
#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>


namespace fs = boost::filesystem;


Superimpose::Superimpose()
{

    this->cmd = Settings::superimpose.command;
    args = Settings::superimpose.settings;
}

Superimpose::~Superimpose()
{

}

void Superimpose::Start()
{

    LOG_INFO << " Starting SuperImpose Module ..." ;
    start = std::chrono::system_clock::now();

    this->args["inr"] = Settings::staticClassificationData.referenceImage;

    int referenceResolution = GetImageResolution ( Settings::staticClassificationData.referenceImage ) ;

    // start for input image
    for ( auto &it :Settings::multidateClassificationDataList )
    {
        for ( auto &image : it.imageList ) // superimpose input image list
        {

            if ( GetImageResolution ( image ) !=  referenceResolution ) // need superimpose
            {
                fs::path imagePath ( image );

                std::string outImagePath = imagePath.replace_extension ( "superimpose.tif" ).string() ;

                this->args["inm"] = image;
                this->args["out"] =  outImagePath ;


                this->outputFile = outImagePath; // to check if output file is created
                this->Run();

                if ( ! this->succeed )
                {
                    LOG_FATAL <<"An Error happened when trying to superimpse  :" << image;
                    std::exit ( 1 );
                }

                image = outImagePath ; // update input list
            }

        }

        for ( auto &image : it.textureList ) // textureList
        {

            if ( GetImageResolution ( image ) !=  referenceResolution ) // need superimpose
            {
                fs::path imagePath ( image );

                std::string outImagePath = imagePath.replace_extension ( "superimpose.tif" ).string() ;

                this->args["inm"] = image;
                this->args["out"] =  outImagePath ;


                this->outputFile = outImagePath; // to check if output file is created
                this->Run();

                if ( ! this->succeed )
                {
                    LOG_FATAL <<"An Error happened when trying to superimpse  :" << image;
                    std::exit ( 1 );
                }

                image = outImagePath ; // update input list
            }

        }

    }
    
    end = std::chrono::system_clock::now();

    Settings::superimpose.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::superimpose.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::superimpose.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

    LOG_INFO << "End SuperImpose Module" ;

}
