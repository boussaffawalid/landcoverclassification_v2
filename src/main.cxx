/*********************************************************************************
*	Program:     Land Cover Classification
*	Language:    C++
*	Date:        $Date$
*	Version:     $Revision$
*	Created By:  Boussaffa Walid
*	Email:       boussaffa.walid@outlook.com
* 	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
*
*	For the licensing terms see LICENSE file inside the root directory.
*	For the list of contributors see AUTHORS file inside the root directory
**********************************************************************************/


#include <cstdlib>
#include <iostream>
#include <string>
#include "app.h"

#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/algorithm/string/replace.hpp>


#include <boost/format.hpp>

#include"logger.h"
#include "settings.h"

#include"filesettings.h"

#include"processingchaine.h"


using namespace std;
namespace po = boost::program_options;
namespace cs = po::command_line_style;


App app;

po::options_description getDescription();


int main ( int argc, char *argv[] )
{

    po::options_description desc = getDescription();
    po::variables_map varmap;
    vector<string> unrecognized;
//allow_unregistered().
    try
    {
        po::parsed_options parsed = po::command_line_parser ( argc, argv )
                                    .options ( desc ).
                                    style ( cs::short_allow_adjacent | cs::short_allow_next| cs::allow_long | cs::allow_short|
                                    cs::long_allow_adjacent | cs::long_allow_next| cs::allow_sticky
				     | cs::allow_dash_for_short| cs::allow_long_disguise ).run();

        //po::store populates the variables_map while po::notify raises any errors encountered, 
	//so varmap can be used prior to any notifications being sent.				     
        po::store ( parsed, varmap );

        if ( varmap.count ( "help" )  || parsed.options.empty() )
        {
	    std::stringstream stream;
            stream << desc;
            string helpMsg = stream.str ();
            boost::algorithm::replace_all (helpMsg, "--", "-");
	    boost::algorithm::replace_all (helpMsg, "[=arg(=1)]", "          ");
	    boost::algorithm::replace_all (helpMsg, "arg", "   ");
            cout << helpMsg << endl;
	    
           // cout << desc ;
            return EXIT_SUCCESS;
        }

        if ( varmap.count ( "version" ) )
        {
            LOG_INFO << app.getProjectVersion();
            return EXIT_SUCCESS;
        }

        if ( varmap.count ( "GenerateParameterFile" ) )
        {
            //TODO
            LOG_INFO << "TODO !! " ;
            return EXIT_SUCCESS;
        }

       // po::notify ( varmap );
        unrecognized = po::collect_unrecognized ( parsed.options, po::include_positional );

        if ( ! unrecognized.empty() )
        {
            LOG_WARNING <<"Unrecognized option: " <<  unrecognized.front() ;
            LOG_WARNING <<"Use -help for details" ;
            return EXIT_FAILURE;
        }
        
        po::notify ( varmap );


    }
    catch ( po::error &error )
    {
        LOG_FATAL << error.what();
        return EXIT_FAILURE;
    }




    FileSettings  importedSettings = FileSettings ( Settings::configFile );

    importedSettings.updateInputDataList();
    importedSettings.updateModulesSettings();


    Settings::Print();
    Settings::Validate();

    ProcessingChaine processing = ProcessingChaine();
    processing.Start();



    return EXIT_SUCCESS;
}


po::options_description getDescription()
{
    //TODO change default value : import value from parameter class
    std::stringstream ss;

    ss << app.getProjectName() << " v" << app.getProjectVersion() << std::endl;
    ss << "Created by "<<app.getProjectVendorName() << ", "<< app.getProjectVendorID() << "  Copyright (C) " << app.getProjectCopyrightYears() << std::endl;
    ss << std::endl <<"Usage: "<< app.getProjectCodeName() << " [option]" ;

    po::options_description desc ( ss.str() );

    po::options_description genearle_options ( "General Options",1,1 );
    genearle_options.add_options()
    ( "help,h", "Display this help message." )

    ( "version,v", "Prints the program version." )

    ( "overwrite",po::value<bool> ( &Settings::isOverwrite )->implicit_value ( true ),
      "Overwriting existing output files [true/false], default value is false " )

    ( "generateConfig,g",po::value<std::string>()->notifier ( &Settings::SetGenerateConfigMethod ),
      "Generate a config file, this file is created in your workspace, [min/all] " )
    ;

    po::options_description classification_options ( "Classification Options",1,1 );
    classification_options.add_options()
    ( "configFile", po::value<string> ( &Settings::configFile )->required()->notifier ( Settings::SetConfigFile ), "Sets the config file path" )

    ( "workspace", po::value<string> ( &Settings::workspace )->required()->notifier ( Settings::SetWorkdir ), "Sets the workspace directory" )

    // ( "OrthoRectification, O",po::value<bool> ( &Parameter::isOrthoRectification )->implicit_value ( true ),
    //   "Ortho-rectify input images list [true/false], default value is false" )

    ( "opticalCalibration",po::value<bool> ( &Settings::isOpticalCalibration )->implicit_value ( true ),
      "Perform optical calibration for input images list [true/false], default value is false" )

    ( "radiometricIndices",po::value<bool> ( &Settings::isRadiometricIndices )->implicit_value ( true ),
      "Compute radiometric indices for input images list [true/false], default value is false" )

    ( "textureExtraction",po::value<bool> ( &Settings::isTextureExtraction )->implicit_value ( true ),
      "Computes textures on every pixel of all channel for the input images list [true/false], default value is false" )

    ( "classification",po::value<bool> ( &Settings::isClassification )->implicit_value ( true ),
      "Performs a classification of the input reference image [true/false], default value is false" )

    ( "statistics",po::value<bool> ( &Settings::isStatistics )->implicit_value ( true ) ,
      "Use statistics file to normalize classification input images [true/false], default value is false" )

    ( "featureSelection",po::value<std::string>()->notifier ( &Settings::SetFeatureSelectionMethod )->implicit_value("all"),
      "Sets features selection method [all/manually/rf/sffs/dos], default value is all" )

    ( "colorMapping",po::value<std::string>()->notifier ( &Settings::SetColorMappingMethod )->implicit_value("image"),
      "Maps input label image to 8-bits RGB using look-up tables [image/custom], default value is image" )

    ( "mask",po::value<bool> ( &Settings::isMask )->implicit_value ( true ),
      "Use a shape file or an image as mask for classification,  [true/false], default value is false" )

    ( "geometryCheck",po::value<bool> ( &Settings::isGeomCheck )->implicit_value ( true ),"check image geometry [true/false], default value is true" )
    ( "regularization",po::value<bool> ( &Settings::isRegularization )->implicit_value ( true ),\
      "Filters the classified image using Majority Voting in a ball shaped neighbordhood. default value is true" )
    
    ("images",  po::value<vector<string> >(&Settings::inputImagesIniSection )->multitoken()->required(), "input images ini sections")
    
    ("session",  po::value<string>(&Settings::outputSubdirs.session ), "session name")
    
    ;

    desc.add ( genearle_options ).add ( classification_options );
    return desc;
}
