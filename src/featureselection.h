/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef FEATURESELECTION_H
#define FEATURESELECTION_H

#include <iostream>
#include <vector>
#include <map>
#include "lcctype.h"

#include <chrono>
#include <ctime>

class FeatureSelection
{

    std::vector<std::string>  inputImageList ;
    
    std::chrono::time_point<std::chrono::system_clock> start, end;


    struct FSImportance
    {
        std::vector< Importance > importance;

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize ( Archive& ar, unsigned int ver ) 
	{
            ar & importance;
        }
    };

    FSImportance fsImportance;

    std::string statisticsfile ;
    std::string shapefile ;
    std::string output;

    bool isRescaleData;
    int nbcompToKeep;

    void begin();


public:

    FeatureSelection();
    ~FeatureSelection();

    void Start();


};

#endif // FEATURESELECTION_H
