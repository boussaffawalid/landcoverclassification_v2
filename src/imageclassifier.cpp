/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "imageclassifier.h"
#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>



using namespace std;

ImageClassifier::ImageClassifier()
{
    this->cmd = Settings::imageClassifier.command;
    args = Settings::imageClassifier.settings;
}

ImageClassifier::~ImageClassifier()
{

}

void ImageClassifier::Start()
{
    LOG_INFO << "Starting Images Classifier ..." ;
    start = std::chrono::system_clock::now();

    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );


    //WARNING  
    // Verify this !! maybe choose only one image  as image to classify !!
    std::string InputImage = Settings::staticClassificationData.selectedFeatures;

    std::string outputImage = sessionDir + "/classification.tif";


    this->args["in"] = InputImage;

    if ( Settings::isStatistics )
        this->args["imstat"] = Settings::staticClassificationData.imagesStatisticsFile;

    if ( Settings::isMask )
        this->args["mask"] = Settings::staticClassificationData.mask;

    this->args["model"] = Settings::staticClassificationData.trainModel;

    this->args["out"] = outputImage;

    this->outputFile = outputImage; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened when trying to classify "<<  this->args["in"] ;
        std::exit ( 1 );
    }

    Settings::staticClassificationData.classifiedImage = outputImage;

    end = std::chrono::system_clock::now();

    Settings::imageClassifier.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::imageClassifier.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::imageClassifier.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

    
    LOG_INFO << "End Images Classifier ." ;
}

