/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "readimageinfo.h"
#include "settings.h"
#include "logger.h"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

ReadImageInfo::ReadImageInfo()
{
    this->cmd = Settings::readImageInfo.command;
    args = Settings::readImageInfo.settings;
}

ReadImageInfo::~ReadImageInfo()
{

}

void ReadImageInfo::Start()
{
    LOG_INFO << " Start checking images geometry ..." ;

    for ( auto it :Settings::multidateClassificationDataList )
    {
        for ( auto image : it.imageList )
        {
            LOG_DEBUG << "checking geom file  for image "<< image;
            //initialize geom file to not exist
            this->succeed  = false ;

            //WARNING  boost::filesystem::exists  not working if the path string if not trimmed !!
            // if geom file exist => cool

            fs::path imagePath ( image );

            std::string geomfile = imagePath.replace_extension ( "geom" ).string();


            if ( fs::exists ( geomfile ) )
            {
                this->succeed  = true ;
                LOG_TRACE << "geom file exit: " << geomfile ;
            }
            //else try to create geom file
            else
            {
                LOG_WARNING <<"Geom file not exit for : " << image;
                LOG_WARNING <<"Trying to create geom file";

                this->args["in"] = image ;
                this->args["outkwl"] = geomfile ;

                this->outputFile = geomfile; // to check if output file is created
                this->Run();
            }

            if ( ! this->succeed )
            {
                LOG_FATAL <<"Error in image :" << image;
                LOG_FATAL <<"Image with no geom file and can generate it !!" ;
                std::exit ( 1 );
            }
        }
    }

    LOG_INFO << " End checking images geometry " ;
}

