/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include "lcctype.h"
#include <iostream>
#include <map>
#include <vector>

 

#include <chrono>
#include <ctime>


class Settings
{

public:



       struct moduleTime {
       std::time_t start;
       std::time_t end;
       long elapsedSeconds;
    } ;
    

    struct ModuleSettings {
        std::string command;
        std::string iniSection;
        std::map <std::string, std::string> settings;
	moduleTime time;
    } ;

    struct MultidateClassificationData {
        std::string iniSection;
        std::string outputFolder;
        std::vector< std::string > imageList;
        std::vector< std::string > radiometricIndicesList;
        std::vector< std::string > textureList;

    };


    struct StaticClassificationData {
        std::string mask; // use one mask for all dates
        std::string referenceImage; //TODO develop method to autodetect reference image
        std::string testVector;    // one test vector for all the date
        std::string trainVector;
        std::string imagesStatisticsFile;
        std::string trainModel;
        std::string classifiedImage;
        std::string colorMapingImage;
        std::string quicklook;

        std::string selectedFeatures;
        std::vector<Importance> featuresImportance;

    };


    static void updateReferenceImage();

    struct OutputSubdirs {
        std::string opticalCalibration;
        std::string radiometricIndices;
        std::string haralicktexture;
        std::string mask;
        std::string session;
    } ;


    static std::vector< std::string > inputImagesIniSection;




    static int nbcompToKeep;

    // overwrite existent output files if exist

    static bool isOverwrite;
    static std::string configFile;
    static std::string workspace;

    static FS_method featureSelectionMethod;
    static GENERATE_CONFIG generateConfigMethod;
    static COLOR_MAPPING_TYPE colorMappingMethod;


    static bool isMask;
    static bool isOrthoRectification;
    static bool isOpticalCalibration;
    static bool isRadiometricIndices;
    static bool isTextureExtraction;
    static bool isClassification;
    static bool isStatistics;
    static bool isGeomCheck;
    static bool isRegularization;

    static void SetFeatureSelectionMethod ( std::string const& val );
    static void SetGenerateConfigMethod ( std::string const&  val );
    static void SetColorMappingMethod ( std::string const&  val );


    static void SetConfigFile ( std::string const& val );
    static void SetWorkdir ( std::string const& val );

    static void Validate();
    static void Print();


    /////////////////////////////////////////////////////////
    static ModuleSettings opticalCalibration;
    static ModuleSettings concatenateImages;
    static ModuleSettings textureExtraction;
    static ModuleSettings rasterization;
    static ModuleSettings radiometricIndices;
    static ModuleSettings readImageInfo;
    static ModuleSettings splitImage;
    static ModuleSettings superimpose;

    static ModuleSettings mapRegularization;
    static ModuleSettings colorMapping;
    static ModuleSettings confusionMatrix;
    static ModuleSettings imagesStatistics;
    static ModuleSettings featureSelection;
    static ModuleSettings imageClassifier;
    static ModuleSettings trainImagesClassifier;
    static ModuleSettings quicklook;

    /////////////////////////////////////////////////////////
    static std::vector<ModuleSettings*> moduleList;

    static std::vector<MultidateClassificationData> multidateClassificationDataList;

    static OutputSubdirs outputSubdirs;
    static StaticClassificationData staticClassificationData;

};

#endif // PARAMETER_H

