/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "computeconfusionmatrix.h"

#include  "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>


namespace fs = boost::filesystem;



ComputeConfusionMatrix::ComputeConfusionMatrix()
{
    this->cmd = Settings::confusionMatrix.command;
    args = Settings::confusionMatrix.settings;
}

ComputeConfusionMatrix::~ComputeConfusionMatrix()
{

}


void ComputeConfusionMatrix::Start()
{

    LOG_INFO << "Starting Compute Confusion Matrix ..." ;

    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );

    std::string outputmatrix = sessionDir + "/ConfusionMatrix.csv";
    std::string logfile = sessionDir + "/ConfusionMatrix.log";

    this->args["in"] = Settings::staticClassificationData.classifiedImage ;
    this->args["ref.vector.in"] = Settings::staticClassificationData.testVector ;
    this->args["out"] = outputmatrix;

    //log execution tarce
    this->logFile = logfile ;


    this->outputFile = outputmatrix; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened when trying Compute Confusion Matrix "  ;
        std::exit ( 1 );
    }

    LOG_INFO << "End Compute Confusion Matrix ." ;

}
