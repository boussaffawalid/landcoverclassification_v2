/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef IMAGECLASSIFIER_H
#define IMAGECLASSIFIER_H


#include"command.h"


class ImageClassifier : public Command
{
public:
    ImageClassifier();
    ~ImageClassifier();

    void Start();
};

#endif // IMAGECLASSIFIER_H
