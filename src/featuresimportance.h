/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef RANDOMFORESTSVECTOR_H
#define RANDOMFORESTSVECTOR_H

#include "otbWrapperTypes.h"
#include "otbListSampleGenerator.h"

#include "otbRandomForestsMachineLearningModel.h"
#include "lcctype.h"







class FeaturesImportance
{

/////////////////////////////////////////////////////


    typedef otb::ListSampleGenerator<otb::Wrapper::FloatVectorImageType, otb::Wrapper::VectorDataType> ListSampleGeneratorType;
    typedef otb::RandomForestsMachineLearningModel<otb::Wrapper::FloatVectorImageType::InternalPixelType,
            ListSampleGeneratorType::ClassLabelType> RandomForestType;

    typedef ListSampleGeneratorType::ListSampleType ListSampleType;
    typedef ListSampleGeneratorType::ListLabelType LabelListSampleType;

    ListSampleType::Pointer trainingListSample;
    LabelListSampleType::Pointer trainingLabeledListSample ;
    
    std::vector<std::string > featureBandList;

public:
    FeaturesImportance(ListSampleType::Pointer trainingSample,
            LabelListSampleType::Pointer trainingLabeledSample, std::vector<std::string > featureList );
    
    ~FeaturesImportance();

    std::vector< Importance > getRandomForestImportance ( );

    std::vector< Importance >  getDOSImportance (  );

    std::vector< Importance >  getSFFSImportance ( );


};

#endif // RANDOMFORESTSVECTOR_H
