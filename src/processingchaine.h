/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#ifndef PROCESSINGCHAINE_H
#define PROCESSINGCHAINE_H

#include "readimageinfo.h"
#include "opticalcalibration.h"
#include "radiometricindices.h"
#include "haralicktextureextraction.h"
#include "mask.h"
#include "superimpose.h"
#include "computeimagesstatistics.h"
#include "trainimagesclassifier.h"
#include "imageclassifier.h"
#include "classificationregularization.h"
#include "computeconfusionmatrix.h"
#include "colormapping.h"
#include "featureselection.h"
#include "quicklook.h"

#include "reporter.h"


class ProcessingChaine
{
    ReadImageInfo _readimageinfo;
    OpticalCalibration _opticalCalibration;
    RadiometricIndices _radiometricIndices;
    HaralickTextureExtraction _haralickTextureExtraction;
    Mask _mask;
    Superimpose _superimpose;

    FeatureSelection _featureSelection;
    ComputeImagesStatistics _computeImagesStatistics;
    TrainImagesClassifier _trainImagesClassifier;
    ImageClassifier _imageClassifier;
    ClassificationRegularization classificationRegularization;
    ComputeConfusionMatrix _computeConfusionMatrix;
    ColorMapping _colorMapping;
    
    Quicklook _quicklook;

public:
    ProcessingChaine();
    ~ProcessingChaine();

    void Start();
};

#endif // PROCESSINGCHAINE_H
