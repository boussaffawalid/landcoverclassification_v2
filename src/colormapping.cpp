/*
 * 	Program:     Land Cover Classification
 * 	Language:    C++
 * 	Date:        $Date$
 * 	Version:     $Revision$
 * 	Created By:  Boussaffa Walid
 * 	Email:       boussaffa.walid@outlook.com
 *  	Copyright (c) 2014 Boussaffa Walid. All rights reserved.
 *
 * 	For the licensing terms see LICENSE file inside the root directory.
 * 	For the list of contributors see AUTHORS file inside the root directory
 */

#include "colormapping.h"

#include <iostream>
#include <string>
#include "settings.h"
#include "logger.h"
#include "tools.h"
#include <boost/filesystem.hpp>



ColorMapping::ColorMapping()
{
    this->cmd = Settings::colorMapping.command;
    this->args = Settings::colorMapping.settings;

}

ColorMapping::~ColorMapping()
{

}

void ColorMapping::Start()
{
    LOG_INFO << "Starting Color Mapping ..." ;
    start = std::chrono::system_clock::now();

    std::string sessionDir = Settings::workspace + "/" + Settings::outputSubdirs.session;
    checkDirectory ( sessionDir );

    std::string output = sessionDir +"/ColorMaping.tif" ;

    this->args["in"] = Settings::staticClassificationData.classifiedImage ;
    this->args["method.image.in"] = Settings::staticClassificationData.referenceImage ;

    this->args["out"] = output;

    this->outputFile = output; // to check if output file is created
    this->Run();

    if ( ! this->succeed )
    {
        LOG_FATAL <<"An Error happened in Color Mapping !" ;
        std::exit ( 1 );
    }

    LOG_INFO << "End Color Mapping" ;
    end = std::chrono::system_clock::now();

    Settings::colorMapping.time.start = std::chrono::system_clock::to_time_t ( start );
    Settings::colorMapping.time.end = std::chrono::system_clock::to_time_t ( end );
    Settings::colorMapping.time.elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds> ( end-start ).count();

    
    Settings::staticClassificationData.colorMapingImage = output;
}
